﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CustomHUB : NetworkManager {
	
	public string PlayerName;

	public void StartupHost()
	{
		SetPort ();
		NetworkManager.singleton.StartHost ();
		PlayerName = GameObject.Find ("InputField").transform.FindChild ("Text").GetComponent<Text> ().text;
	}

	public void JoinGame()
	{
		SetIPAddress ();
		SetPort ();
		NetworkManager.singleton.StartClient ();
	}

	void SetIPAddress()
	{
		//string ipaddress = GameObject.Find ("InputField").transform.FindChild ("Text").GetComponent<Text> ().text;
		//NetworkManager.singleton.networkAddress = ipaddress;
	}

	void SetPort()
	{
		NetworkManager.singleton.networkPort = 7777;
	}

	void OnLevelWasLoaded(int level)
	{
		if (level == 0) 
		{
			setupMenuscenebutton ();
		}

		else 
		{
			setupOtherscenebutton();
		}
	
	}

	void setupMenuscenebutton()
	{
		GameObject.Find ("StartHost").GetComponent<Button> ().onClick.RemoveAllListeners ();
		GameObject.Find ("StartHost").GetComponent<Button> ().onClick.AddListener (StartupHost);

		GameObject.Find ("FindHost").GetComponent<Button> ().onClick.RemoveAllListeners ();
		GameObject.Find ("FindHost").GetComponent<Button> ().onClick.AddListener (JoinGame);
	}

	void setupOtherscenebutton()
	{
		GameObject.Find ("Disconnect").GetComponent<Button> ().onClick.RemoveAllListeners ();
		GameObject.Find ("Disconnect").GetComponent<Button> ().onClick.AddListener (NetworkManager.singleton.StopHost);

	}

}
