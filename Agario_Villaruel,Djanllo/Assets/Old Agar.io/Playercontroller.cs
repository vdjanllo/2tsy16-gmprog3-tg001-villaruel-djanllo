﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Playercontroller : MonoBehaviour {

	public enum celltype
	{
		verysmall = 10,
		small = 50,
		medium = 100,
		mediumlarge = 150,
		category1 = 200,
		category2 = 250,
		largecell = 300,
	};


	//public celltype Celltype;

	float initialcamsize;
	public float playerwalkspeed;
	private Vector3 playerdirection;
	public float playerturnSpeed;


	// Use this for initialization
	void Start () {
		//playersize = this.transform.localScale;
		//Celltype = celltype.verysmall;
	}
	
	// Update is called once per frame
	void Update () {

		//split
		if (Input.GetKey (KeyCode.Space)) 
		{
			this.GetComponent<Gamemanager>().split();
		}

		if (Input.GetKey (KeyCode.W)) 
		{
			this.GetComponent<Gamemanager>().eject();
		}

		//player current position
		Vector3 currentPosition = transform.position;
	

			//Target position
		Vector3 moveToward = Camera.main.ScreenToWorldPoint( Input.mousePosition );

			//Determines player direction
		playerdirection = moveToward - currentPosition;
		playerdirection.z = 0; 
		playerdirection.Normalize();

		//player rotates to target position
		float targetAngle = Mathf.Atan2(playerdirection.y, playerdirection.x) * Mathf.Rad2Deg;
		transform.rotation = 
			Quaternion.Slerp( transform.rotation, 
			                 Quaternion.Euler( 0, 0, targetAngle ), 
			                 playerturnSpeed * Time.deltaTime );

		//Player moves to the target position 
		Vector3 target = playerdirection + currentPosition;
		transform.position = Vector3.Lerp( currentPosition, target, playerwalkspeed/(((this.transform.localScale.x - 1) * 0.3f)/2) * Time.deltaTime );

		//wall restriction
		if (this.gameObject.transform.position.x > 100) 
		{
			this.gameObject.transform.position = new Vector3(100, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
		}

		if (this.gameObject.transform.position.x < -100) 
		{
			this.gameObject.transform.position = new Vector3(-100, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
		}

		if (this.gameObject.transform.position.y > 100) 
		{
			this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, 100, this.gameObject.transform.position.z);
		}

		if (this.gameObject.transform.position.y < -100) 
		{
			this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, -100, this.gameObject.transform.position.z);
		}
	}
	
}
