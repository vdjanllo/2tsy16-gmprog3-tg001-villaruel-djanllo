﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
public class playernetworksetup : NetworkBehaviour {

	[SerializeField] Camera playercam;
	[SerializeField] AudioListener audiolistener;
	[SerializeField] GameObject playercontrolled;
	public List<Material> mat = new List<Material>();



	// Use this for initialization
	void Start () {
	
		playercontrolled.GetComponent<SpriteRenderer>().material = mat[Random.Range(0, mat.Count)];

		if (isLocalPlayer) 
		{
			//GameObject.Find("Main Camera").SetActive(false);

			playercontrolled.GetComponent<Playercontroller>().enabled = true;
			playercam.enabled = true;
			audiolistener.enabled = true;
		}

		if (!isLocalPlayer) 
		{

		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
