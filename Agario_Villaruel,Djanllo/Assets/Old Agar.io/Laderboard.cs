﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Laderboard : MonoBehaviour {

	public List<LeaderboardClass> leaderboard = new List<LeaderboardClass>(); 
	public Text leaderboardText;
	public float time;
	// Use this for initialization
	void Start () {
	
//		leaderboard.Add (new LeaderboardClass ("f", 3));
//		leaderboard.Add (new LeaderboardClass ("t", 1));
//		leaderboard.Add (new LeaderboardClass ("h", 7));
//		leaderboard.Sort ();
//
//		foreach (LeaderboardClass leader in leaderboard) 
//		{
//			print(leader.name + " " + leader.score);
//		}

	}
	
	// Update is called once per frame
	void Update () 
	{
		leaderboardText = GameObject.Find ("Leaderboard").GetComponentInChildren<Text> ();

		time += Time.deltaTime;

		if (time >= 3) 
		{
			leaderboardText.text = "";
			GameObject.Find("Playerobject(Clone)").GetComponent<PlayerID>().addtoList();

			leaderboardText.text += "Leader Board \n";
			foreach (LeaderboardClass leader in leaderboard) 
			{
				leaderboardText.text += "" + leader.name + " " + leader.score + "\n";
			}

			time = 0;
			leaderboard.Clear ();
		}

	}
}
