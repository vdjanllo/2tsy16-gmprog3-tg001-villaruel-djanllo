﻿using UnityEngine;
using System.Collections;
using System;

public class LeaderboardClass : IComparable<LeaderboardClass> {

	public string name;
	public float score;
	public string playerid;

	public LeaderboardClass(string newname, float newscore, string newplayerid)
	{
		name = newname;
		score = newscore;
		playerid = newplayerid;
	}

	public int CompareTo(LeaderboardClass other)
	{
		if (other == null) 
		{
			return 1;
		}

		return (int)(score - other.score);
	}

}
