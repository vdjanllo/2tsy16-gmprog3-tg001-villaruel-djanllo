﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerID : NetworkBehaviour {

	[SyncVar]public string playername;
	private NetworkInstanceId playernetid;
	public Transform mytransform;

	// Use this for initialization
	public override void OnStartLocalPlayer () 
	{
		GetNetIdentity ();
		SetIdentity ();
		GameObject.Find ("Networkmanager").GetComponent<Laderboard> ().leaderboard.Add(new LeaderboardClass(playername, 0, playernetid.ToString()));
	}

	void Awake()
	{
		//mytransform = transform;
	}

	// Update is called once per frame
	void Update () 
	{
		if (mytransform.name == "" || mytransform.name == "Player") 
		{
			SetIdentity();
		}
	}

	public void addtoList()
	{
		GameObject.Find ("Networkmanager").GetComponent<Laderboard> ().leaderboard.Add(new LeaderboardClass(playername, GetComponentInChildren<Gamemanager>().score, playernetid.ToString()));
	}

	[Client]
	void GetNetIdentity()
	{
		playernetid = GetComponent<NetworkIdentity> ().netId;
		CmdTellServerMyIdentity (MakeUniqueName ());
	}


	void SetIdentity()
	{
		if (!isLocalPlayer) {
			mytransform.name = playername;
		}

		else
		{
			mytransform.name = MakeUniqueName();
		}
	}

	string MakeUniqueName()
	{
		string uniqueName = "" + GameObject.Find("Networkmanager").GetComponent<CustomHUB>().PlayerName;
		return uniqueName;
	}

	[Command]
	void CmdTellServerMyIdentity(string name)
	{
		playername = name;
	}

}
