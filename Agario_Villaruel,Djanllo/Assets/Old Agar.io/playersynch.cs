﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class playersynch : NetworkBehaviour {

	[SyncVar]
	private Vector3 syncpos;
	private Vector3 syncsize;
	public List<Gamemanager> otherplayer = new List<Gamemanager>();

	[SerializeField] List<Transform> playertransform = new List<Transform>();
	[SerializeField] float lerprate = 15;

	private Vector3 lastpos;
	private Vector3 lastsize;
	private float threshold = 0.5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		TransmitPosition ();
		TransmitSize ();
		lerpPosition ();
	}

	public void addNewtransform(Transform newCell)
	{
		playertransform.Add (newCell);
	}

	void lerpPosition()
	{
		if (!isLocalPlayer) 
		{
			for(int count = 0; count < playertransform.Count; count++)
			{
			playertransform[count].position = Vector3.Lerp(playertransform[count].position, syncpos, Time.deltaTime * lerprate);
			playertransform[count].localScale = Vector3.Lerp(playertransform[count].localScale, syncsize, Time.deltaTime * lerprate);
			}
		}
	}

	[Command]
	void CmdProvidePositionToServer(Vector3 pos, Vector3 size)
	{
		syncpos = pos;
		syncsize = size;
	}

	[ClientCallback]
	void TransmitPosition()
	{
		for (int count = 0; count < playertransform.Count; count++) 
		{
			if (isLocalPlayer && Vector3.Distance (playertransform[count].position, lastpos) > threshold) 
			{
				CmdProvidePositionToServer (playertransform[count].position, playertransform[count].localScale);
				lastpos = playertransform[count].position;
			}
		}

	}

	[ClientCallback]
	void TransmitSize()
	{
		for (int count = 0; count < playertransform.Count; count++) 
		{
			if (isLocalPlayer && Vector3.Distance (playertransform[count].localScale, lastsize) > threshold) 
			{
				CmdProvidePositionToServer (playertransform[count].position, playertransform[count].localScale);
				lastpos = playertransform[count].localScale;
			}
		}

	}

	[ClientCallback]
	void TransimitGeneralManager()
	{
		for(int count = 0; count < playertransform.Count; count++)
		{
		//playertransform[count].GetComponent<Gamemanager>() = 
		}
	}
}
