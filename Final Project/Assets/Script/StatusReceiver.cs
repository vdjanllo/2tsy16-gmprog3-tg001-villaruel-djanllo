﻿using UnityEngine;
using System.Collections;

public class StatusReceiver : MonoBehaviour {

	public bool IsSlowed = false;
	public bool IsDot = false;
	public float timeelapsed;
	static float initialSpeed;
	NavMeshAgent nma;
	Health health;

	// Use this for initialization
	void Start () {
		nma = GetComponent <NavMeshAgent> ();
		health = GetComponentInChildren <Health> ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnEnable ()
	{
		this.AddEventListener<OnStatusReceive> (StatusReceive);
	}

	void OnDisable ()
	{
		this.RemoveEventListener<OnStatusReceive> (StatusReceive);
	}

	void DamageOverTime()
	{
		health.decrease ();
	}

	void StatusReceive (OnStatusReceive eventDetails)
	{
		//Slow
		initialSpeed = nma.speed;
		if (eventDetails.Slowed && !IsSlowed) {
			eventDetails.SlowDuration -= Time.deltaTime;
			float ReducedValue = nma.speed * eventDetails.SpeedReduction;
			float SlowedValue = nma.speed - ReducedValue;
			nma.speed = SlowedValue;
			IsSlowed = true;
		}

		Timer (IsSlowed, eventDetails.SlowDuration);

		if (!IsSlowed) {
			nma.speed = initialSpeed;
		}

		//DOT
		if (eventDetails.Dot && !IsDot) {
			//eventDetails.DotDuration -= Time.deltaTime;
			IsDot = true;
		}

		if (IsDot)
		{
			InvokeRepeating ("DamageOverTime", 1.0f, 0.5f);
		}

	/*	if (IsDot) {
			eventDetails.DotDuration -= Time.deltaTime;
			timeelapsed += Time.deltaTime;
			Debug.Log (timeelapsed);
			if (timeelapsed > eventDetails.DotInterval) {
				health.decrease ();
				//timeelapsed = 0;
			}
		}
		Timer (IsDot, eventDetails.DotDuration);*/
	}

	void Timer (bool Status, float Duration)
	{
		if (Status && Duration > 0) {
			Duration -= Time.deltaTime;
		} else {
			Status = false;
		}
	}
}
