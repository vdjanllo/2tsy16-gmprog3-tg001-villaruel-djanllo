﻿using UnityEngine;
using System.Collections;

public class ProjectileFire : MonoBehaviour {

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			OnStatusReceive e = new OnStatusReceive ();
			e.Dot = true;
			e.DotDuration = 1.0f;
			e.DotInterval = 0.5f;
			other.gameObject.RaiseEvent<OnStatusReceive> (e);
		}

	}

}
