﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	public GameObject Target;
	public Transform Front;
	private float enemydist;
	SphereCollider sc;

	// Use this for initialization
	void Start ()
	{
		sc = GetComponent<SphereCollider> ();
	}
		
	// Update is called once per frame
	void Update ()
	{
		if (Target != null)
		{
			enemydist = Vector3.Distance (Front.transform.position, Target.transform.position);
			if (enemydist < sc.radius)
			{
				LookAtEnemy ();
			}
		}
	}

	void LookAtEnemy ()
	{
		transform.LookAt(Target.transform);
	}
}
