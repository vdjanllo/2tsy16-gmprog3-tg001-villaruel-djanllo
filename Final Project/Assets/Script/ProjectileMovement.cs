﻿using UnityEngine;
using System.Collections;

public class ProjectileMovement : MonoBehaviour
{
	public GameObject Target;
	public float speed;
	private float elapsedtime;
	//Tower tower;

	// Use this for initialization
	void Start ()
	{
		//tower = GetComponent <Tower> ();
	}
	
	// Update is called once per frame
	void Update ()
	{

		elapsedtime += Time.deltaTime;
		if (elapsedtime >= 2.0f)
		{
			Destroy(this.gameObject);
			elapsedtime = 0;
		}
		if (Target != null)
		{
		Movement ();
		}
	}

	void Movement ()
	{
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (this.transform.position, Target.transform.position, step);
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Enemies")
		{
			Destroy (gameObject);
		}
	}
}
