﻿using UnityEngine;
using System.Collections;

public class OnTargetRequest : GameEvent
{
	public GameObject Target;
}

public class ProjectileSpawner : MonoBehaviour
{
	public GameObject Projectile;
	public Transform SpawnPoint;
	public float FiringRate;
	private float timeelapsed;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		timeelapsed += Time.deltaTime;

		OnTargetRequest e = new OnTargetRequest ();
		this.RaiseEvent<OnTargetRequest> (e);
		this.GetComponent<Rotate>().Target = e.Target;
		//this.GetComponent<ProjectileSplash>().target = e.Target;

		if (FiringRate <= timeelapsed && e.Target != null) {
			// Send event here w/ a class data
			// After the event you now 	 have a target
			GameObject bulletClone = (GameObject)Instantiate (Projectile, SpawnPoint.transform.position, Quaternion.identity);
			timeelapsed = 0;
			bulletClone.GetComponent<ProjectileMovement> ().Target = e.Target;
			//bulletClone.GetComponent<Tower>().ProjectileSpeed = speed;
			//Set Target
			// RaiseEvent
		}
	}
}
