﻿using UnityEngine;
using System.Collections;

public class Buildplace : MonoBehaviour 
{
	// The Tower that should be built
	public GameObject[] towerPrefab;
	public int x;

	public void Button_Click_ArrowTower()
	{
		x = 0;
	}

	public void Button_Click_FireTower()
	{
		x = 2;
	}

	public void Button_Click_IceTower()
	{
		x = 1;
	}

	public void Button_Click_CannonTower()
	{
		x = 3;
	}

	void OnMouseUpAsButton() 
	{
		// Build Tower above Buildplace
		GameObject g = (GameObject)Instantiate(towerPrefab[x]);
		g.transform.position = transform.position + Vector3.up;
	}
}