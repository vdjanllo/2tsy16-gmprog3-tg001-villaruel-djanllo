﻿using UnityEngine;
using System.Collections;

public class TowersArray : MonoBehaviour {
    public GameObject[] Towers;
    public GameObject BuildButtons;

    public GameObject CurrentSelectedObject;
    // Use this for initialization
    void Start ()
    {
        BuildButtons.SetActive(false);
        CurrentSelectedObject = null;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (CurrentSelectedObject != null)
            BuildButtons.SetActive(true);
        else
            BuildButtons.SetActive(false);
	}

	public void Build(int index)
	{


	}

    public void BuildArrow()
    {
        Instantiate(Towers[0], new Vector3(CurrentSelectedObject.transform.position.x, 
                                            CurrentSelectedObject.transform.position.y, 
                                            CurrentSelectedObject.transform.position.z), Quaternion.identity);
        CurrentSelectedObject = null;
    }

    public void BuildDPS()
    {

        Instantiate(Towers[1], new Vector3(CurrentSelectedObject.transform.position.x, 
                                            (CurrentSelectedObject.transform.position.y), 
                                             CurrentSelectedObject.transform.position.z), Quaternion.identity);
        CurrentSelectedObject = null;
    }
    public void BuildSlow()
    {

        Instantiate(Towers[2], new Vector3(CurrentSelectedObject.transform.position.x, 
                                            (CurrentSelectedObject.transform.position.y), 
                                             CurrentSelectedObject.transform.position.z), Quaternion.identity);
        CurrentSelectedObject = null;
    }
    public void BuildSplash()
    {

        Instantiate(Towers[3], new Vector3(CurrentSelectedObject.transform.position.x, 
                                            (CurrentSelectedObject.transform.position.y), 
                                             CurrentSelectedObject.transform.position.z), Quaternion.identity);
        CurrentSelectedObject = null;
    }
}
