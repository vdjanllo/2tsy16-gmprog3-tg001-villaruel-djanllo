﻿using UnityEngine;
using System.Collections;

public class TargetAcquierer : MonoBehaviour
{

	private GameObject[] enemies;
	public GameObject Base;
	Tower1 tower;

	// Use this for initialization
	void Start ()
	{
		tower = GetComponent<Tower1> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	void OnEnable ()
	{
		this.AddEventListener<OnTargetRequest> (TargetRequest);
	}

	void OnDisable ()
	{
		this.RemoveEventListener<OnTargetRequest> (TargetRequest);
	}

	void TargetRequest (OnTargetRequest eventDetails)// Event details as param
	{
		enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		if (enemies.Length <= 0)
			return;
		
		float nearestdist = 10000;
		for (int i = 0; i < enemies.Length; i++) {
			float enemydistance = Mathf.Abs (Vector3.Distance (this.transform.position, enemies [i].transform.position));
			
			if (enemydistance < tower.Range) {
				float closesttobase = Mathf.Abs (Vector3.Distance (Base.transform.position, enemies [i].transform.position));
				if (closesttobase < nearestdist) {
					nearestdist = closesttobase;
					eventDetails.Target = enemies [i];
				}
			}
		}
	}

	// method to recieve event
	// recieve data
	// put code for detection here
	// set data.Target = detectedTarget
}
