﻿using UnityEngine;
using System.Collections;

public class UIComponent : MonoBehaviour {

	public GameObject Context;

	private GameObject prevContext = null;

	private bool locked = false;
	public bool Locked
	{
		get { return locked; }
		set
		{
			if (value == locked) return;
			locked = value;
			if (locked) OnLock();
			else OnUnlock();
		}
	}

	private bool visible = false;
	public bool Visible
	{
		get { return visible; }
		set
		{
			if (value == visible) return;
			visible = value;
			if (visible) OnShow();
			else OnHide();
		}
	}

	void Start()
	{
		Locked = false;
		Visible = true;
	}

	protected void OnUiUpdate()
	{
		if (Context != prevContext)
		{
			OnContextUpdated(Context, prevContext);
		}

		prevContext = Context;
	}

	protected virtual void OnShow()
	{
		gameObject.SetActive(true);
	}

	protected virtual void OnHide()
	{
		gameObject.SetActive(false);
	}

	protected virtual void OnLock()
	{

	}

	protected virtual void OnUnlock()
	{

	}

	protected virtual void OnContextUpdated(GameObject context, GameObject previousContext)
	{

	}
}
