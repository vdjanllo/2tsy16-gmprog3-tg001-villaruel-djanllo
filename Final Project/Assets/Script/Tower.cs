﻿using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour 
{
	// The Bullet
	public GameObject bulletPrefab;
	Rotate rotate;

	void Start()
	{
		rotate = GetComponent<Rotate> ();
	}

	void Update ()
	{
	}

	void OnTriggerEnter(Collider co) 
	{
		// Was it a Monster? Then Shoot it
		if (co.GetComponent<Enemy>()) 
		{
			GameObject g = (GameObject)Instantiate(bulletPrefab, transform.position, Quaternion.identity);
		//	g.GetComponent<Bullet>().target = co.transform.position;
			rotate.Target = g.GetComponent<Bullet>().target;
		}
	}

	void OnTriggerStay(Collider co) 
	{
		// Was it a Monster and still in range? Then Shoot it
		if (co.GetComponent<Enemy>()) 
		{
			
		}
	}
}