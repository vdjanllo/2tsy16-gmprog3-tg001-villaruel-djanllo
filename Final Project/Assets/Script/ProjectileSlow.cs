﻿using UnityEngine;
using System.Collections;

public class ProjectileSlow : MonoBehaviour {

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Enemy") {
			OnStatusReceive e = new OnStatusReceive ();
			e.SpeedReduction = 0.25f;
			e.Slowed = true;
			e.SlowDuration = 3.0f;
			other.gameObject.RaiseEvent<OnStatusReceive> (e);
		}

	}
}
