﻿using UnityEngine;
using System.Collections;

public class Selector : MonoBehaviour
{
    //public static GameObject CurrentSelectedObject;
    //so that only one selected object at a time.
    TowersArray TowersArrayScript;

    // Use this for initialization
    void Start()
    {
        TowersArrayScript = GameObject.Find("ScriptManager").GetComponent<TowersArray>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {
        if (TowersArrayScript.CurrentSelectedObject == null)
            TowersArrayScript.CurrentSelectedObject = gameObject;
        else if (TowersArrayScript.CurrentSelectedObject == this.gameObject)
            TowersArrayScript.CurrentSelectedObject = null;
    }
}
