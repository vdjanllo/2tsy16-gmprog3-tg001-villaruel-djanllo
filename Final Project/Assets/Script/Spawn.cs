﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {
	// The Enemy that should be spawned
	public GameObject[] EnemyPrefab;
	public int WaveNumber;
	public int EnemyCount;
	public float WaveWaitTime;
	public float WaveStart;

	// Spawn Delay in seconds
	public float interval = 2;
	
	// Use this for initialization
	void Start() {
		InvokeRepeating("SpawnNext", interval, interval);
	}

	void Update()
	{
		if (WaveStart > 0) 
		{
			WaveStart -= Time.deltaTime;
		}

		if (WaveStart > 0) 
		{
			CancelInvoke ("SpawnNext");
		} 

		else
		{
			if(!IsInvoking("SpawnNext"))
			InvokeRepeating("SpawnNext", interval, interval);
		}

		if (EnemyCount <= 0) 
		{
			WaveNumber += 1;
			EnemyCount = WaveNumber + 5;
			WaveStart = WaveWaitTime;
		}
	}

	void SpawnNext() {
		if (EnemyCount > 0) 
		{
			if (WaveNumber % 3 == 0 && EnemyCount == 1) 
			{
			Instantiate (EnemyPrefab [2], transform.position, Quaternion.identity);
			}

			else
			{
				Instantiate (EnemyPrefab [Random.Range(0,2)], transform.position, Quaternion.identity);
			}

			EnemyCount -= 1;
		}
	}
}