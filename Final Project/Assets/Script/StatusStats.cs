﻿using UnityEngine;
using System.Collections;

public class OnStatusReceive : GameEvent
{
	public float Damage;
	public float SpeedReduction;
	public float SlowDuration;
	public float DotDuration;
	public float DotInterval;
	public float DotDamage;
	public bool Slowed;
	public bool Dot;

}

public class StatusStats : MonoBehaviour {

}
