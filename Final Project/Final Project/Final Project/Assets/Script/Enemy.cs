﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour 
{
	public enum EnemyType
	{
		flying,
		grounded,
		boss
	};

	[SerializeField]
	EnemyType enemytype;

	// Use this for initialization
	void Start () 
	{
		GetComponent<NavMeshAgent> ().speed = GetComponent<NavMeshAgent> ().speed + GameObject.Find ("Spawn").GetComponent<Spawn> ().WaveNumber - 1;
		// Navigate to Castle
		GameObject castle = GameObject.Find("Castle");
		if (castle)
			GetComponent<NavMeshAgent>().destination = castle.transform.position;
	}
	void OnTriggerEnter(Collider co) 
	{
		// If castle then deal Damage, destroy self
		if (co.name == "Castle") {
			co.GetComponentInChildren<Health>().decrease();
			Destroy(gameObject);
		}
	}
}