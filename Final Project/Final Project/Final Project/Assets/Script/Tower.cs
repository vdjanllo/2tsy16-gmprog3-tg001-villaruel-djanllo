﻿using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour 
{
	// The Bullet
	public GameObject bulletPrefab;

	void Update ()
	{

	}

	void OnTriggerEnter(Collider co) 
	{
		// Was it a Monster? Then Shoot it
		if (co.GetComponent<Enemy>()) 
		{
			GameObject g = (GameObject)Instantiate(bulletPrefab, transform.position, Quaternion.identity);
			g.GetComponent<Bullet>().target = co.transform;
		}
	}

	void OnTriggerStay(Collider co) 
	{
		// Was it a Monster and still in range? Then Shoot it
		if (co.GetComponent<Enemy>()) 
		{
			
		}
	}
}